import Vue from 'vue'
import Router from 'vue-router'
import HeaderContent from '@/components/layout/HeaderContentLayout'
import HeaderMenuContent from '@/components/layout/HeaderMenuContentLayout'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: HeaderContent,
      props: { currentComponent: 'Login' }
    },
    {
      path: '/root',
      name: 'LoginRoot',
      component: HeaderContent,
      props: { currentComponent: 'Login' }
    },
    {
      path: '/register',
      name: 'Register',
      component: HeaderContent,
      props: { currentComponent: 'Register' }

    },
    {
      path: '/attendance',
      name: 'Attendance',
      component: HeaderMenuContent,
      props: { currentComponentProp: 'Attendance' }
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: HeaderMenuContent,
      props: { currentComponentProp: 'Calendar' }

    },
    {
      path: '/todo',
      name: 'ToDo',
      component: HeaderMenuContent,
      props: { currentComponentProp: 'ToDo' }

    },
    {
      path: '/request',
      name: 'Reequest',
      component: HeaderMenuContent,
      props: { currentComponentProp: 'Request' }

    },
    {
      path: '/setting',
      name: 'Setting',
      component: HeaderMenuContent,
      props: { currentComponentProp: 'Setting' }
    },
    {
      path: '/root-menu',
      name: 'RootMenu',
      component: HeaderContent,
      props: { currentComponent: 'RootMenu' }
    },
  ]
})
