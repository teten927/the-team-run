import Vue from 'vue'
import Vuex from 'vuex'
import { realtimeFetchChatInfo, fetchOrganization, realtimeFetchMembersInfo, fetchRequest, onAuth, updateMyInfo, addChatMessage } from '../api/firebase';

Vue.use(Vuex);

function getDefaultState() {
  return {
    initAttendanceModalTrigger: false,  // 未出勤時、最初のページ表示で出勤モーダルを表示するトリガー
    isAuthed: false,                    // セッション取得が終了したかどうかの判定
    isLoading: false,                   // ロード画面表示のトリガー
    isRoot: false,                      // root権限かどうか
    resultMsgModalInfo: {               // 結果表示画面(モーダル)表示の内容
      result: null,                     // 結果
      message: null, 
      width: null,                   // 表示メッセージ
      onClick: null,                     // [OK]ボタン押下後の処理
    },
    membersInfo: {},                    // 同じ組織内のメンバーの情報
    loginUser: null,                    // ログインした自分自身のLoginID
    userOrganization: null,             // ログインした自分自身が所属する組織名
    chatInfo: [],                       // 自組織のチャットの情報(過去500件)
    chatNoticeInfo: false,
    request: [],                        // 申請一覧(組織のAdministratorのみ使用)
    isHideHeader: false,
  }
}

export default new Vuex.Store({
  state: getDefaultState(),

  getters: {
    initAttendanceModalTrigger(state) {
      return state.initAttendanceModalTrigger;
    },
    isAuthed(state) {
      return state.isAuthed;
    },
    isLoading(state) {
      return state.isLoading;
    },
    isRoot(state) {
      return state.isRoot;
    },
    resultMsgModalInfo(state) {
      return state.resultMsgModalInfo;
    },
    loginUser(state) {
      return state.loginUser;
    },
    userOrganization(state) {
      return state.userOrganization;
    },
    membersInfo(state) {
      return state.membersInfo;
    },
    chatInfo(state) {
      return state.chatInfo;
    },
    chatNoticeInfo(state) {
      return state.chatNoticeInfo;
    },
    request(state) {
      return state.request;
    },
    isHideHeader(state) {
      return state.isHideHeader;
    },
    currentUserInfo(state) {
      return state.membersInfo[state.loginUser];
    }
  },

  mutations: {
    initialize(state) {
      Object.assign(state, getDefaultState())
    },
    setInitAttendanceModalTrigger(state, trigger) {
      state.initAttendanceModalTrigger = trigger;
    },
    setIsAuthed(state, isAuthed) {
      state.isAuthed = isAuthed;
    },
    setIsLoading(state, isLoading) {
      state.isLoading = isLoading;
    },
    setIsRoot(state, isRoot) {
      state.isRoot = isRoot;
    },
    setResultMsgModalInfo(state, { result, message, width, onClick }) {
      state.resultMsgModalInfo = {
        result,
        message,
        width,
        onClick,
      }
    },
    setMembersInfo(state, membersInfo) {
      state.membersInfo = membersInfo;
    },
    setUserInfo(state, userInfo) {
      state.membersInfo[state.loginUser] = userInfo;
    },
    setLoginUser(state, loginuUser) {
      state.loginUser = loginuUser;
    },
    setUserOrganization(state, userOrganization) {
      state.userOrganization = userOrganization;
    },
    setChatInfo(state, chatInfo) {
      state.chatInfo = chatInfo;
    },
    setChatNoticeInfo(state, chatNoticeInfo) {
      state.chatNoticeInfo = chatNoticeInfo;
    },
    setRequest(state, request) {
      state.request = request;
    },
    setIsHideHeader(state, isHideHeader) {
      state.isHideHeader = isHideHeader;
    },
  },

  actions: {
    async onAuth(context) {
      await onAuth((user) => {
        if (user) {
          context.commit("setLoginUser", user.email);
          context.commit("setIsAuthed", true);
        }
        else context.commit("initialize");
      });
    },
    async fetchMembersInfo(context) {
      const commitMembersInfo = (tmpMembersInfo) => {
        let membersInfo = {};
        tmpMembersInfo.forEach(info => {
          let tmpTodo = [];
          if (info.todo) info.todo.forEach(todo => {
            tmpTodo.push({
              key: todo.key,
              value: todo.value,
              status: todo.status
            });
          })
          membersInfo[info.id] = {
            id: info.id,
            fullname: info.fullname,
            organization: info.organization,
            location: info.location,
            defaultLocation: info.defaultLocation,
            comment: info.comment,
            status: info.status,
            attendanceTime: checkIsTimeStamp(info.attendanceTime) && info.attendanceTime.toDate(),
            isAdmin: info.isAdmin,
            todo: tmpTodo
          }
          if (info.image) membersInfo[info.id].image = info.image;
        })
        context.commit("setMembersInfo", membersInfo);
      }
      await realtimeFetchMembersInfo({ updateFunc: commitMembersInfo });
    },
    async updateMyInfo(context) {
      const loginUser = context.getters.loginUser;
      const membersInfo = context.getters.membersInfo;
      await updateMyInfo({ newInfo: membersInfo[loginUser] });
    },
    async fetchChatInfo(context) {
      const commitChatInfo = chatInfo => {
        let tmpChatInfo = [];
        chatInfo.forEach(info => {
          tmpChatInfo.push(
            {
              user: info.user,
              date: checkIsTimeStamp(info.date) && info.date.toDate(),
              message: info.message
            }
          )
        })
        context.commit("setChatInfo", tmpChatInfo)
      };
      await realtimeFetchChatInfo({ updateFunc: commitChatInfo });
    },
    async addChatMessage(_, message) {
      await addChatMessage({ message });
    },
    async fetchOrganizationName(context) {
      const organizationName = await fetchOrganization();
      context.commit("setUserOrganization", organizationName);
    },
    async fetchRequest(context) {
      const commitRequestInfo = requestInfo => {
        let tmpRequestInfo = [];
        requestInfo.forEach(info => {
          tmpRequestInfo.push(
            {
              data: info.data,
              date: checkIsTimeStamp(info.date) && info.date.toDate(),
              key: info.key,
              request: info.request,
              user: info.user
            }
          )
        })
        context.commit("setRequest", tmpRequestInfo);
      }
      await fetchRequest({ updateFunc: commitRequestInfo });
    }
  }

})

function checkIsTimeStamp(timestamp) {
  return typeof timestamp === "object" && timestamp.seconds != null && timestamp.nanoseconds != null && timestamp
}