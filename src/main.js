import Vue from 'vue';
import router from'./router';
import App from './App.vue';
import VModal from 'vue-js-modal';
import VueMq from 'vue-mq';
import store from './store';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

var firebaseConfig = {
  apiKey: "AIzaSyC1qK5UlWvcW1iIi27zoiJ1vTlSi0h0JhY",
  authDomain: "the-team-run.firebaseapp.com",
  projectId: "the-team-run",
  storageBucket: "the-team-run.appspot.com",
  messagingSenderId: "908804646295",
  appId: "1:908804646295:web:d736895dd70053521590d2",
  measurementId: "G-V13R2QD8BW"
};
firebase.initializeApp(firebaseConfig);

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);

// emulator setting
// firebase.auth().useEmulator("http://localhost:9099");
// firebase.firestore().useEmulator("localhost", 8080);
// firebase.functions().useEmulator("localhost", 5001);
//

Vue.use(VModal);
Vue.use(VueMq, {
  breakpoints: {
    sp: 500,
    tb: 1200,
    pc: Infinity,
  },
  defaultBreakpoint: 'sp'
})

Vue.config.productionTip = false

// グローバル登録：カスタムディレクティブ
Vue.directive('focus', {
  // フック関数
  inserted: function (el) {
    // ディレクティブフックに引数でelを渡し、
    // 要素にフォーカスを当てる
    el.focus()
  }
})

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
