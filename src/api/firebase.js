import firebase from 'firebase/compat/app'
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { Exception } from 'sass';

export async function onAuth(func) {

  return new Promise(resolve => {
    firebase.auth().onAuthStateChanged(user => {
      func(user);
      resolve();
    });
  })
}

export async function login({ id, password }) {
  return await firebase.auth().signInWithEmailAndPassword(id, password);
}

export async function logout() {
  return await firebase.auth().signOut();
}


// ★★★下記、サイト公開用に処理を隠蔽。
export async function updatePassword({ oldPassword, newPassword }) {
  // const user = firebase.auth().currentUser;
  // var credential = firebase.auth.EmailAuthProvider.credential(
  //   user.email,
  //   oldPassword
  // );
  // user.reauthenticateWithCredential(credential);
  // return await firebase.auth().currentUser.updatePassword(newPassword);
  console.log(oldPassword + newPassword);
  throw new Error();
}

export async function fetchOrganizationsByCode({ organizationCode }) {
  const organization = await firebase.firestore().collection('organizations').where('code', '==', organizationCode).get();
  return organization.docs[0].data();
}

export async function fetchOrganizationsByName({ organizationName }) {
  const tmpOrganizations = await firebase.firestore().collection('organizations').orderBy("name").startAt(organizationName).endAt(organizationName + '\uf8ff').get();
  const organizations = [];
  tmpOrganizations.docs.forEach(doc => { organizations.push(doc.data()) });
  return organizations;
}

export async function realtimeFetchMembersInfo({ updateFunc }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const datas = firebase.firestore().collection('users').where('organization', '==', organizationCode);
  datas.onSnapshot(querySnapShot => {
    let usersInfo = [];
    querySnapShot.docs.forEach(doc => { usersInfo.push(doc.data()) });
    updateFunc(usersInfo);
  })
  const tmpUsersInfo = await datas.get();
  let usersInfo = [];
  tmpUsersInfo.docs.forEach(doc => { usersInfo.push(doc.data()) });
  return usersInfo;
}

export async function updateMyInfo({ newInfo }) {
  const user = firebase.auth().currentUser;
  await firebase.firestore().collection('users').doc(user.uid).update(newInfo);
}

export async function toFalseMyAdmin() {
  const user = firebase.auth().currentUser;
  await firebase.firestore().collection('users').doc(user.uid).update({
    isAdmin: false
  });
}

export async function toTrueMembersAdmin({ users }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const usersDoc = await firebase.firestore().collection('users').where('organization', '==', organizationCode).where('id', 'in', users).get();
  const batch = firebase.firestore().batch();
  usersDoc.docs.forEach((doc) => {
    batch.update(doc.ref, { isAdmin: true });
  });
  await batch.commit();
}

export async function deleteUser({ deletedUser }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const usersDoc = await firebase.firestore().collection('users').where('organization', '==', organizationCode).where('id', '==', deletedUser).get();
  const batch = firebase.firestore().batch();
  usersDoc.docs.forEach((doc) => {
    batch.delete(doc.ref);
  });
  await batch.commit();
}

export async function fetchOrganization() {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const organizationInfo = await firebase.firestore().collection('organizations').doc(organizationCode).get();
  return organizationInfo.data().name;
}

export async function realtimeFetchChatInfo({ updateFunc }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const chatInfo = firebase.firestore().collection('chat').doc(organizationCode);
  chatInfo.onSnapshot(querySnapShot => {
    updateFunc(querySnapShot.data().chats);
  })
  return (await chatInfo.get()).data().chats;
}

export async function addChatMessage({ message }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  await firebase.firestore().collection('chat').doc(organizationCode).update({
    chats: firebase.firestore.FieldValue.arrayUnion({ user: user.email, message, date: firebase.firestore.Timestamp.now() })
  });
}

export async function fetchCalendar({ userId, month }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const users = await firebase.firestore().collection('users').where('organization', '==', organizationCode).get();
  const docId = users.docs.find(element => element.data().id === userId).id;
  const calendar = await firebase.firestore().collection('users').doc(docId).collection('calendar').where('month', '==', month).get();
  return calendar.docs[0].data().calendar;
}

export async function fetchCalendarAllUser({ yearMonths }) {
  const subLists = [];
  while (yearMonths.length) {
    subLists.push(yearMonths.splice(0, 10));
  }

  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const users = await firebase.firestore().collection('users').where('organization', '==', organizationCode).get();
  const calendars = {};
  await Promise.all(users.docs.map(async doc => {
    await Promise.all(subLists.map(async list => {
      const id = doc.data().id;
      await firebase.firestore().collection('users').doc(doc.id).collection('calendar').where('month', 'in', list).get().then((calendar) => {
        const data = calendar.docs.map((doc) => doc.data());
        if (calendars[id]) calendars[id] = calendars[id].concat(data);
        else calendars[id] = data;
      });
    }))
  }))
  return calendars;
}

export async function addToDo({ value }) {
  const user = firebase.auth().currentUser;
  const userDoc = firebase.firestore().collection('users').doc(user.uid);

  await firebase.firestore().runTransaction(async transaction => {
    let todo = { key: user.email + firebase.firestore.Timestamp.now(), value: value, status: 'todo' };
    const userToDo = (await transaction.get(userDoc)).data().todo;
    userToDo.unshift(todo);
    transaction.update(userDoc, { todo: userToDo });
  });
}

export async function deleteToDo({ key }) {
  const user = firebase.auth().currentUser;
  const userDoc = firebase.firestore().collection('users').doc(user.uid);

  await firebase.firestore().runTransaction(async transaction => {
    const userToDo = (await transaction.get(userDoc)).data().todo;
    const deletedToDo = userToDo.filter(element => element.key !== key);
    transaction.update(userDoc, { todo: deletedToDo });
  });
}

export async function changeToDo({ todo }) {
  const user = firebase.auth().currentUser;
  const userDoc = firebase.firestore().collection('users').doc(user.uid);

  await userDoc.update({
    todo: todo
  });
}

export async function fetchRequest({ updateFunc }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const request = firebase.firestore().collection('request').doc(organizationCode);
  request.onSnapshot(querySnapShot => {
    updateFunc(querySnapShot.data().request);
  })
  return (await request.get()).data().request;
}

export async function deleteRequest({ key }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const requestDoc = firebase.firestore().collection('request').doc(organizationCode);

  await firebase.firestore().runTransaction(async transaction => {
    const request = (await transaction.get(requestDoc)).data().request;
    const deletedRequest = request.filter(element => element.key !== key);
    transaction.update(requestDoc, { request: deletedRequest });
  });
}

export async function addFixAttendanceRequest({ day, oldAttendance, oldLeave, oldIsVacation, newAttendance, newLeave, newIsVacation }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const time = firebase.firestore.Timestamp.now();
  const data = {
    date: (day && firebase.firestore.Timestamp.fromDate(day)) || null,
    oldAttendance: (oldAttendance && firebase.firestore.Timestamp.fromDate(oldAttendance)) || null,
    oldLeave: (oldLeave && firebase.firestore.Timestamp.fromDate(oldLeave)) || null,
    oldIsVacation: oldIsVacation || false,
    newAttendance: (newAttendance && firebase.firestore.Timestamp.fromDate(newAttendance)) || null,
    newLeave: (newLeave && firebase.firestore.Timestamp.fromDate(newLeave)) || null,
    newIsVacation: newIsVacation || false
  };
  await firebase.firestore().collection('request').doc(organizationCode).update({
    request: firebase.firestore.FieldValue.arrayUnion({
      key: user.email + time, user: user.email, request: "fix", date: time, data: data
    })
  });
}

export async function addVacationRequest({ date }) {
  const user = firebase.auth().currentUser;
  const myInfo = await firebase.firestore().collection('users').doc(user.uid).get();
  const organizationCode = myInfo.data().organization;
  const time = firebase.firestore.Timestamp.now();
  await firebase.firestore().collection('request').doc(organizationCode).update({
    request: firebase.firestore.FieldValue.arrayUnion({
      key: user.email + time, user: user.email, request: "vacation", date: time, data: {
        date: firebase.firestore.Timestamp.fromDate(date)
      }
    })
  });
}

export async function loginRoot({ id, password }) {
  let isRoot = false;
  const docs = await firebase.firestore().collection("root-users").get();
  docs.forEach((doc) => {
    if (id === doc.data().id) isRoot = true;
  })

  if (!isRoot) throw new Exception('No root user');

  return await firebase.auth().signInWithEmailAndPassword(id, password);
}