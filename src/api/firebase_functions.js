import firebase from 'firebase/compat/app';

export async function addJoinRequest({ user, password, organizationCode, fullname }) {
    const createJoinFn = firebase.app().functions('asia-northeast1').httpsCallable('createJoinRequest');
    await createJoinFn({ user: user, password: password, organization: organizationCode, fullname: fullname });
}

export async function acceptJoinUserRequest({ key }) {
    const acceptJoinUserRequestFn = firebase.app().functions('asia-northeast1').httpsCallable('acceptJoinUserRequest');
    await acceptJoinUserRequestFn({ key: key });
}

export async function acceptAttendanceRequest({ key }) {
    const updateAttendanceTimeFn = firebase.app().functions('asia-northeast1').httpsCallable('acceptAttendanceRequest');
    await updateAttendanceTimeFn({ key: key });
}

export async function rejectRequest({ key }) {
    const rejectRequestFn = firebase.app().functions('asia-northeast1').httpsCallable('rejectRequest');
    await rejectRequestFn({ key: key });
}

export async function updateOffVacation({ month, day }) {
    const updateOffVacationFn = firebase.app().functions('asia-northeast1').httpsCallable('updateCalendarByUser');
    await updateOffVacationFn({ month: month, day: day, isVacation: false });
}

export async function updateLocation({ month, day, location }) {
    const updateLocationFn = firebase.app().functions('asia-northeast1').httpsCallable('updateCalendarByUser');
    await updateLocationFn({ month: month, day: day, location: location });
}

export async function createOrganization({ code, name, userEmail, userFullname, userPassword }) {
    const createOrganizationFn = firebase.app().functions('asia-northeast1').httpsCallable('createOrganizationWithUser');
    await createOrganizationFn({ code: code, name: name, userEmail: userEmail, userFullname: userFullname, userPassword: userPassword });
}

export async function deleteOrganization({ code, name }) {
    const deleteOrganizationFn = firebase.app().functions('asia-northeast1').httpsCallable('deleteOrganization');
    await deleteOrganizationFn({ code: code, name: name });
}